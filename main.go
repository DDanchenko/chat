package main

import (
	"flag"
	"fmt"
)

func main() {
	var isServer bool
	var host string
	var port int
	var name string
	flag.BoolVar(&isServer, "server", false, "is server")
	flag.StringVar(&host, "host", "localhost", "host")
	flag.IntVar(&port, "port", 8080, "port")
	flag.StringVar(&name, "name", "noname", "client name")

	flag.Parse()

	if isServer {
		serveServer(fmt.Sprintf("%s:%d", host, port))
	} else {
		serveClient(fmt.Sprintf("%s:%d", host, port), name)
	}
}
