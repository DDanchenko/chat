package main

import (
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"path/filepath"
)

func uploadHandler(w http.ResponseWriter, r *http.Request) {
	filePath := r.URL.Query().Get("filename")
	if filePath == "" {
		fmt.Printf("no filename in upload request")
	} else {
		fmt.Printf("uploading file %s\n", filePath)
	}
	file, err := os.Create(filePath)
	if err != nil {
		panic(err)
	}
	defer file.Close()

	//n, err := io.Copy(file, r.Body)
	n, err := copyBuffer(file, r.Body)
	if err != nil {
		log.Print(err)
		http.Error(w, err.Error(), 400)
		os.Remove(filePath)
	} else {
		w.Write([]byte(fmt.Sprintf("%d bytes are recieved.\n", n)))
	}

}

func upload(addr string, filePath string) error {
	_, fileName := filepath.Split(filePath)
	file, err := os.Open(filePath)
	if err != nil {
		return err
	}
	defer file.Close()

	res, err := http.Post("http://"+addr+`?filename="`+fileName+`"`, "binary/octet-stream", file)
	if err != nil {
		return err
	}
	defer res.Body.Close()
	message, _ := ioutil.ReadAll(res.Body)

	if res.StatusCode != 200 {
		return fmt.Errorf(string(message))
	} else {
		fmt.Printf(string(message))
	}
	return nil
}

func copyBuffer(dst io.Writer, src io.Reader) (written int64, err error) {
	var buf []byte
	if buf == nil {
		buf = make([]byte, 32*1024)
	}
	for {
		if written > 1000000000 {
			return written, fmt.Errorf("file is bigger than 1Gb")
		}
		nr, er := src.Read(buf)
		if nr > 0 {
			nw, ew := dst.Write(buf[0:nr])
			if nw > 0 {
				written += int64(nw)
			}
			if ew != nil {
				err = ew
				break
			}
			if nr != nw {
				err = io.ErrShortWrite
				break
			}
		}
		if er == io.EOF {
			break
		}
		if er != nil {
			err = er
			break
		}
	}
	return written, err
}
