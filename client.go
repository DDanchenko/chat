package main

import (
	"bufio"
	"fmt"
	"log"
	"net/url"
	"os"
	"strings"

	"github.com/gorilla/websocket"
)

func serveClient(server, name string) {
	u := url.URL{Scheme: "ws", Host: server, Path: "/enter"}
	log.Printf("connecting to %s", u.String())

	connection, _, err := websocket.DefaultDialer.Dial(u.String(), nil)
	if err != nil {
		log.Fatal("dial:", err)
		return
	}
	defer connection.Close()

	// push name:
	if err := connection.WriteMessage(websocket.TextMessage, []byte(name)); err != nil {
		fmt.Printf("Can't send name: %s\n", err.Error())
		return
	}

	// read worker:
	go func(connection *websocket.Conn) {
		m := message{}
		for {
			if err := connection.ReadJSON(&m); err == nil {
				fmt.Printf("[%d:%d]%s:%s\n", m.Time.Minute(), m.Time.Second(), m.Author, m.Message)
			} else {
				fmt.Printf("Can't read message:%s\n", err.Error())
				break
			}
		}
	}(connection)

	consolereader := bufio.NewReader(os.Stdin)
	for {
		var response string
		response, err := consolereader.ReadString('\n')
		//n, _ := fmt.Scanln(&response)
		if err == nil {
			if response != "exit" {
				if strings.HasPrefix(response, "upload") == false {
					m := &message{Author: name, Message: response}
					if err := connection.WriteJSON(m); err != nil {
						fmt.Printf("Error send message: %s\n", err.Error())
						break
					}
				} else {

					var filePath string

					fmt.Sscanf(response, "upload %s\n", &filePath)
					go func(filePath string) {
						fmt.Printf("Try upload file %s\n", filePath)
						err := upload(server+"/upload", filePath)
						if err != nil {
							fmt.Printf("error uploading file: %s\n", err)
						} else {
							fmt.Printf("File was successfly downloaded\n")
						}
					}(filePath)
				}
			} else {
				break
			}
		}
	}

}
