
Usage of ./chat:
  -host string

	host (default "localhost")

  -name string

	client name (default "noname")

  -port int

	port (default 8080)

  -server

	is server

чтобы создать сервер

./chat -server true

чтобы законектится к локальному серверу

./chat -name "имя"

чтобы законектится к удаленному серверу 

./chat -name "имя" -host "ip сервера"


чтобы зааплоадить файл в чате написать upload <путь файла>

путь может быть как относительный (отн. рабочей папки клиента)
и полный относительно root

чтобы выйти - в чате exit или ctrl+C