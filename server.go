package main

import (
	"container/list"
	"fmt"
	"log"
	"net/http"
	"sync"
	"time"

	"github.com/gorilla/websocket"
)

var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
}

type message struct {
	Author  string `json:"author"`
	authorP *client
	Message string    `json:"message"`
	Time    time.Time `json:"time"`
}

type chat struct {
	messages    *list.List
	clients     map[string]*client
	messageFlow chan *message
	clientFlow  chan *client
}

func newChat() *chat {
	ch := &chat{}
	ch.messages = list.New()
	ch.clients = make(map[string]*client)
	ch.messageFlow = make(chan *message, 20)
	ch.clientFlow = make(chan *client)
	return ch
}

func (th *chat) messageToChatFlowNoBlock(messages ...*message) {
	for _, message := range messages {
		select {
		case th.messageFlow <- message:
		default:
			// we can't block chat goroutine
		}
	}
}

func (th *chat) messageToChatFlow(messages ...*message) {
	for _, message := range messages {
		th.messageFlow <- message
	}
}

//chat gouroutine
func (th *chat) goChat() {
	disconnectedClients := make([]string, 0, 10)
	for {
		select {
		case message, ok := <-th.messageFlow:
			if ok == true {
				// check message queue overfull
				th.messages.PushFront(message)
				for th.messages.Len() > 10 {
					th.messages.Remove(th.messages.Back())
				}
				// distribute message to all:
				for _, cl := range th.clients {
					if cl != nil {
						cl.sendMessage(message)
					}
				}
			}
		case client, ok := <-th.clientFlow:
			if ok == true {
				th.clients[client.name] = client
				mess := fmt.Sprintf("Client connected: %s\n", client.name)
				th.messageToChatFlowNoBlock(&message{Author: "chat", Message: mess, Time: time.Now()})
				for e := th.messages.Back(); e != nil; e = e.Prev() {
					client.sendMessage(e.Value.(*message))
				}
			}
		default:
			for _, cl := range th.clients {
				if cl.connection == nil {
					disconnectedClients = append(disconnectedClients, cl.name)
				}
			}
			for _, clName := range disconnectedClients {
				mess := fmt.Sprintf("Client disconnected: %s\n", clName)
				th.messageToChatFlowNoBlock(&message{Author: "chat", Message: mess, Time: time.Now()})
				delete(th.clients, clName)
			}
			disconnectedClients = disconnectedClients[:0]
			time.Sleep(time.Second)
		}
	}
	close(th.messageFlow)
}

// client goroutine
func (th *chat) goClient(connection *websocket.Conn) {
	defer connection.Close()
	t, nameMessage, err := connection.ReadMessage()
	if t == websocket.TextMessage {
		name := string(nameMessage)
		// check client present
		if _, ok := th.clients[name]; ok == true {
			mess := fmt.Sprintf("The client with name %s already in chat", name)
			connection.WriteMessage(websocket.TextMessage, []byte(mess))
			return
		}
		// init client
		cl := newClient(name, connection)
		defer cl.disconnect()
		th.clientFlow <- cl

		var wg sync.WaitGroup

		// recieve worker:
		wg.Add(1)
		go func(cl *client, ch *chat, wg *sync.WaitGroup) {
			defer wg.Done()
			//recieve messages:
			var err error
			for {
				m := &message{}
				err = connection.ReadJSON(m)
				if err == nil {
					//log.Printf("%s:\n", m.Message)
					if m.Author == name {
						m.authorP = th.clients[name]
						m.Time = time.Now()
						ch.messageToChatFlow(m)
					}
				} else {
					break
				}
			}
			if err != nil {
				log.Printf("client error %s:%s\n", cl.name, err.Error())
			} else {
				log.Printf("send worker done %s\n", cl.name)
			}
		}(cl, th, &wg)

		// send worker
		wg.Add(1)
		go func(cl *client, messageThread <-chan *message, wg *sync.WaitGroup) {
			defer wg.Done()
			// send all messages
			for message := range messageThread {
				log.Printf("[%d:%d]%s:%s", message.Time.Minute(), message.Time.Second(), message.Author, message.Message)
				if err := cl.connection.WriteJSON(message); err != nil {
					log.Printf("Error send message %v\n", message)
					break
				}
			}
			log.Printf("send worker done %s\n", cl.name)
		}(cl, cl.messageSendFlow, &wg)

		// wait for end work
		wg.Wait()

	} else {
		fmt.Println(err.Error())
	}
}

type client struct {
	name            string
	state           string // connected/disconnected
	connection      *websocket.Conn
	messageSendFlow chan *message
}

func newClient(name string, conn *websocket.Conn) *client {
	return &client{name: name, state: "connected", connection: conn, messageSendFlow: make(chan *message, 20)}
}

func (th *client) sendMessage(messages ...*message) {
	defer func() {
		if rec := recover(); rec != nil {
			log.Printf("problem with send messages to %s %s\n", th.name, recover())
		}
	}()
	if th.messageSendFlow != nil {
		for _, message := range messages {
			select {
			case th.messageSendFlow <- message:
			default:
				// we can't block chat goroutine
			}
		}
	}
}

func (th *client) disconnect() {
	th.connection.Close()
	th.connection = nil
	th.state = "disconnected"
	close(th.messageSendFlow)
}

func serveServer(addr string) {
	fmt.Println("Starting server ...")
	var chat = newChat()
	http.HandleFunc("/enter", func(w http.ResponseWriter, r *http.Request) {
		// init connection
		fmt.Println("\nConnecting Addr:", r.RemoteAddr)
		conn, err := upgrader.Upgrade(w, r, nil)
		if err != nil {
			fmt.Println(err)
			return
		}
		go chat.goClient(conn)
	})

	http.HandleFunc("/upload", func(w http.ResponseWriter, r *http.Request) {
		fmt.Println("Upload Addr:", r.RemoteAddr)
		uploadHandler(w, r)
	})

	go chat.goChat()

	if err := http.ListenAndServe(addr, nil); err != nil {
		fmt.Println(err)
	}
}
